application = eloquentlog
network = $(application)-net

repository_host = gitlab.com
repository_user = $(application)

repositories = \
	$(application)-console-api \
	$(application)-cli \
	$(application)-web

srv ?= $(shell pwd)/srv

rc:
ifeq ($(shell ps -ef | grep "[d]ockerd" | awk {'print $2'}),)
	@echo "docker daemon does not exist" && exit 1
endif

checkout:
	@if [ ! -d "$(srv)" ]; then \
		mkdir $(srv); \
	fi
	@for repository in $(repositories); do \
		if [ ! -d "$(srv)/$$repository" ]; then \
			git clone git@$(repository_host):$(repository_user)/$$repository.git \
				$(srv)/$$repository && cd $(srv)/$$repository; \
			git remote rename origin gitlab; \
		else \
			cd $(srv)/$$repository; \
			git pull gitlab trunk; \
		fi \
	done
.PHONY: checkout

network: rc
	@if docker network inspect $(network) >/dev/null 2>&1; then \
		echo "network named as $(network) already exists"; \
	else \
		docker network create \
			-o "com.docker.network.bridge.name"="$(network)" $(network); \
	fi
.PHONY: network

init:
	@FETCH=true $(MAKE) up
	@docker-compose ps
	@sleep 3
	@$(MAKE) build
	@sleep 3
	@$(MAKE) join
	@$(MAKE) deploy:postgresql
	@$(MAKE) deploy:redis
	@$(MAKE) deploy:console-api
	@$(MAKE) deploy:web
	@$(MAKE) info
.PHONY: init

build:
	@docker-compose -f docker-compose.eloquentlog.yml -p $(application) build
	@docker tag eloquentlog-console-api-server:latest \
		localhost:5000/eloquentlog-console-api-server:latest
	@docker push localhost:5000/eloquentlog-console-api-server
	@docker tag eloquentlog-console-api-worker:latest \
		localhost:5000/eloquentlog-console-api-worker:latest
	@docker push localhost:5000/eloquentlog-console-api-worker
.PHONY: build

up: rc network
ifeq ($(shell [ "true" = "$${FETCH}" ] && echo $$?), 0)
	make checkout
endif
	@docker-compose -f docker-compose.yml -p $(application) up -d
.PHONY: up

down: rc
	@docker-compose -f docker-compose.yml -p $(application) down
.PHONY: down

clean: rc
	@docker-compose -f docker-compose.yml -p $(application) down \
		--rmi local --volumes
	@docker network rm $(network)
.PHONY: clean

start: up
.PHONY: start

stop: rc
	@docker-compose -f docker-compose.yml -p $(application) stop
.PHONY: stop

restart: rc
	@docker-compose -f docker-compose.yml -p $(application) restart
.PHONY: restart

node\:join:
	@docker container exec -it $(application)-manager docker swarm init \
		>/dev/null
	@docker container exec -it $(application)-manager docker network create \
		--driver=overlay --attachable $(application) >/dev/null
	@token="$$( \
		docker container exec -it $(application)-manager \
			docker swarm join-token worker | grep token | awk '{print $$5}' \
	)" && \
		docker container exec -it "$(application)-db" \
			docker swarm join --token "$${token}" $(application)-manager:2377; \
		docker container exec -it "$(application)-web" \
			docker swarm join --token "$${token}" $(application)-manager:2377; \
		docker container exec -it "$(application)-worker" \
			docker swarm join --token "$${token}" $(application)-manager:2377;
.PHONY: node\:join

join: node\:join
.PHONY: join

node\:leave:
	@docker container exec -it "$(application)-worker" \
		docker swarm leave
	@docker container exec -it "$(application)-web" \
		docker swarm leave
	@docker container exec -it "$(application)-db" \
		docker swarm leave
	@docker container exec -it $(application)-manager docker swarm leave --force
.PHONY: node\:leave

leave: node\:leave
.PHONY: leave

node\:info:
	@echo "# $(shell date --rfc-3339=ns)"
	@echo "# network: ${application}"
	@echo ""
	@echo "[postgresql:master]"
	@echo "docker container exec -it $(shell docker container exec -it \
		$(application)-manager docker service ps ${application}-postgresql_master \
		--no-trunc \
		--filter 'desired-state=running' \
		--format '{{.Node}} docker exec -it {{.Name}}.{{.ID}}')"
	# @echo ""
	# @echo "[postgresql:slave]"
	# @echo "$$(docker container exec -it $(application)-manager \
	# 	docker service ps $(application)-postgresql_slave \
	# 	--no-trunc \
	# 	--filter 'desired-state=running' \
	# 	--format '{{.Node}} docker exec -it {{.Name}}.{{.ID}}')" | \
	# 	while read line; do \
	# 		echo "docker container exec -it $${line}"; \
	# 	done
	@echo ""
	@echo "[redis:master]"
	@echo "docker container exec -it $(shell docker container exec -it \
		$(application)-manager docker service ps ${application}-redis_master \
		--no-trunc \
		--filter 'desired-state=running' \
		--format '{{.Node}} docker exec -it {{.Name}}.{{.ID}}')"
	@echo ""
	# @echo "[redis:slave]"
	# @echo "$$(docker container exec -it $(application)-manager \
	# 	docker service ps $(application)-redis_slave \
	# 	--no-trunc \
	# 	--filter 'desired-state=running' \
	# 	--format '{{.Node}} docker exec -it {{.Name}}.{{.ID}}')" | \
	# 	while read line; do \
	# 		echo "docker container exec -it $${line}"; \
	# 	done
	# @echo ""
	@echo "[console-api:server]"
	@echo "$$(docker container exec -it $(application)-manager \
		docker service ps $(application)-console-api_server \
		--no-trunc \
		--filter 'desired-state=running' \
		--format '{{.Node}} docker exec -it {{.Name}}.{{.ID}}')" | \
		while read line; do \
			echo "docker container exec -it $${line}"; \
		done
	@echo ""
.PHONY: node\:info

info: node\:info
.PHONY: info

deploy\:postgresql:
	@echo "* deploying $(application)-postgresql service containers..."
	@echo
	@docker container exec -it $(application)-manager docker stack deploy \
		-c ./stack/$(application)-postgresql.yml $(application)-postgresql
.PHONY: deploy\:postgresql

deploy\:redis:
	@echo "* deploying $(application)-redis service containers..."
	@echo
	@docker container exec -it $(application)-manager docker stack deploy \
		-c ./stack/$(application)-redis.yml $(application)-redis
.PHONY: deploy\:redis

deploy\:console-api:
	@echo "* deploying $(application)-console-api service containers..."
	@echo
	@docker container exec -it $(application)-manager docker stack deploy \
		-c ./stack/$(application)-console-api.yml $(application)-console-api
.PHONY: deploy\:console-api

deploy\:web:
	@echo "* deploying $(application)-web service containers..."
	@echo
	@docker container exec -it $(application)-manager docker stack deploy \
		-c ./stack/$(application)-web.yml $(application)-web
.PHONY: deploy\:web

deploy\:visualizer:
	@echo "* deploying $(application)-visualizer service containers..."
	@echo
	@docker container exec -it $(application)-manager docker stack deploy \
		-c ./stack/$(application)-visualizer.yml $(application)-visualizer
.PHONY: deploy\:visualizer
# }}}
